# README #

Hier werden nach und nach die Meldungen des [PhantomBot](https://phantombot.tv/) übersetzt, da die [andere, existierende Übersetzung](https://github.com/X00LA/PhantomBot-German-Translation) nicht mehr aktualisiert wird.

Außerdem scheint in der alten Übersetzung mehr vorhanden zu sein als die Sprachpakete. Hier gibt es nur die reinen Sprachpakete, um Fehler bei den Bot-Funktionen vollständig zu vermeiden.

**Hinweis:** Es werden hiermit (soweit ich das sehe) nur die Ausgaben vom Bot-Übersetzt. Das Interface bleibt Englisch, da ich dafür vermutlich zu „tief“ in den Bot eingreifen müsste und bei Updates etwas kaputt gehen könnte.

**Aktuell getestete Bot-Version: 2.3.7.1**; „inkompatibel“ mit 2.3.6.1 in Bezug auf die Black- und Whitelists. Sollte allerdings lediglich zu fehlenden Übersetzungen in der alten Version kommen.


## Installation via git

1. Gehe in deinem **PhantomBot-Ordner** nach `scripts/lang/german`
2. `git clone git@bitbucket.org:hellgrn/phantombot_lang_german.git german`
3. Schreibe als Bot-Admin in deinem Chat: `!lang german`

Weitere Aktualisierungen erfolgen dann üblicherweise via `git pull`.

Der Bot nutzt nun das deutsche Sprachpaket und muss dafür nicht neu geladen werden.


## Manuelle Installation

1. Lade das Sprachpaket herunter ([Download](https://bitbucket.org/hellgrn/phantombot_lang_german/downloads/))
2. Entpacke den Inhalt in deinem **PhantomBot-Ordner** nach `scripts/lang/`
3. Schreibe als Bot-Admin in deinem Chat: `!lang german`

Der Bot nutzt nun das deutsche Sprachpaket und muss dafür nicht neu geladen werden.


## Eigene Anpassungen

Du kannst die Übersetzungen jederzeit anpassen. Öffne einfach die Datien mit einem Texteditor deiner Wahl (unter Windows z.B. [Notepad++](https://notepad-plus-plus.org/)) und ändere die Texte so ab, wie sie dir am besten gefallen.

Das kann durchaus sinnvoll sein, wenn man die Formulierungen des eigenen Bots etwas persönlicher gestalten möchte. Oder man ersetzt z.B. in `systems/pontSystem.js` den Befehl `!points` in den Übersetzungen zu dem eigenen Alias, damit die Zuschauer in den Hilfestellungen den bevorzugten Befehl zu lesen bekommen.


## Korrekturen

Falls du Fehler findest, kannst du sie, wie oben beschrieben, natürlich selbst korrigieren. Ich würde mich freuen, wenn du mir die Korrekturen zukommen lässt, damit ich sie hier für alle übernehmen kann.
