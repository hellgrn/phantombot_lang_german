$.lang.register('gambling.need.points',     'Du hast nicht genug $1!');
$.lang.register('gambling.error.max',       'Du darfst nur um maximal $1 spielen!');
$.lang.register('gambling.error.min',       'Du musst ein Minimum von $1 setzen, um spielen zu können!');
$.lang.register('gambling.lost',            '$1 würfelte $2 und hat dadurch $3 verloren. Neuer Punktestand: $4'); // Nutze $4 für die neue Punktzahl
$.lang.register('gambling.won',             '$1 würfelte $2 und hat dadurch $3 gewonnen! Neuer Punktestand: $4'); // Nutze $4 für die neue Punktzahl
$.lang.register('gambling.usage',           'Verwendung: !gamble [Punkt-Anzahl]');
$.lang.register('gambling.set.max.usage',   'Verwendung: !gamblesetmax [Anzahl]');
$.lang.register('gambling.set.max',         'Maximaler Glücksspieleinsatz auf $1 gesetzt.');
$.lang.register('gambling.set.min.usage',   'Verwendung: !gamblesetmin [Anzahl]');
$.lang.register('gambling.set.min',         'Minimaler Glücksspieleinsatz auf $1 gesetzt.');
$.lang.register('gambling.win.range.usage', 'Verwendung: !gamblesetwinningrange [Spanne]');
$.lang.register('gambling.win.range',       'Glücksspiel-Gewinnspanne auf $1-100 und -Verlustrate auf 0-2$ gesetzt.');
$.lang.register('gambling.percent.usage',   'Verwendung: !gamblesetgainpercent [amount]');
$.lang.register('gambling.percent',         'Glücksspiel-Gewinnrate auf $1% gesetzt.');
